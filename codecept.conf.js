exports.config = {
  tests: 'y/*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://www.iim.fr',
      show:true
    }
  },
  include: {},
  bootstrap: null,
  mocha: {},
  name: 'bookstore'
}
//{
//  "output": "./output",
//  "helpers": {
//    "Puppeteer": {
//      "url": "http://todomvc.com/examples/react/",
//      "waitForAction": 500
//    },
//    "MyPuppeteer": {
//      "require": "./mypuppeteer_helper.js"
//    }
//  },
//  "include": {
//    "I": "./steps_file.js"
//  },
//  "mocha": {},
//  "bootstrap": false,
//  "teardown": null,
//  "hooks": [],
//  "tests": "./*_test.js",
//  "timeout": 10000,
//  "name": "todoreact"
//}