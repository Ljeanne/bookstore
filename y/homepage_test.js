
Feature('Homepage');

Before((I) => {
  I.amOnPage('/');
});


Scenario('test something', (I) => {
  I.seeInTitle('IIM Paris – Grande École du Digital');
});


Scenario('developpement-web', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/developpement-web/');
});

Scenario('communication-digitale-ebusiness', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/communication-digitale-ebusiness/');
});

Scenario('creation-design', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/creation-design/');
});

Scenario('axes-metiers/animation-3d', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/animation-3d/');
});

Scenario('jeux-video', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/jeux-video/');
});
Scenario('axes-metiers/jeux-video', (I) => {
 I.amOnPage('https://www.iim.fr/axes-metiers/jeux-video/');
});

Scenario('alternance', (I) => {
 I.amOnPage('https://www.iim.fr/entreprises-debouches/alternance/');
 I.see('Elodie Guéneau');
 I.see('elodie.gueneau@devinci.fr');
 I.clearField('elodie.gueneau@devinci.fr');
 
  
});
//Scenario('edit todo', (I) => {
  //I.createTodo('write a review');
  //I.see('write a review', '.todo-list');  
  //I.doubleClick('write a review');
  //I.pressKey(['Control', 'a']);
  //I.fillField({css: ':focus'}, 'write old review');
  //I.pressKey('Enter');
  //I.see('write old review', '.todo-list');



